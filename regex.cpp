/*
 * File for boost::regex experiments
 *
 * By PB
 *
 * How to compile: g++ -g -Wall -o regex.run regex.cpp -lboost_regex --std=c++11
 */

#include <boost/regex.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <iostream>
#include <string>

bool is_valid_isocode(std::string s)
{
    return true; // Dummy for checking with isocode database
}

bool is_known_currency(std::string s)
{
    return true;
}

bool is_waluta(const std::string& str){
    boost::smatch match;
    static const boost::regex rx("^\\s*([A-Z]{3})\\s+(\\d+(?:,\\d{1,3})?)\\s*$");//TODO: do disallow leading and trailing  0's
    if(boost::regex_match(str, match, rx)){
        if(is_valid_isocode(match[1])){
            std::cout << "Got: " << match[1] << " " << std::stod(boost::replace_all_copy(std::string(match[2]), ",", ".")) << std::endl;
            return true;    // match[i] is std::string with 3 letters currency code,
                            //match[2] is a 3 digit precision string containing exchange ratio for this currency
        }
    }
    return false;
}

bool is_donation(const std::string& str)
{
    boost::smatch match;
    static const boost::regex rx("^\\s*([^\\s]+(?:\\s+[^\\s]+)*)\\s+(\\d+(?:,\\d{1,3})?)\\s+([A-Z]{3})\\s*$");
    if(boost::regex_match(str, match, rx)){
        if(is_known_currency(match[3])){
            std::cout << "Got: " << match[1] << " " << std::stod(boost::replace_all_copy(std::string(match[2]), ",", ".")) << " " << match[3] << std::endl;
            return true;    // match[i] is std::string with 3 letters currency code,
                            //match[2] is a 3 digit precision string containing exchange ratio for this currency
        }
    }
    return false;
}

bool is_zapytanie(const std::string& str)
{
    boost::smatch match;
    static const boost::regex rx("^\\s*(\\d+(?:,\\d{1,3})?)\\s*(\\d+(?:,\\d{1,3})?)\\s*$");
     if(boost::regex_match(str, match, rx)){
        double n1, n2;
        n1 = std::stod(boost::replace_all_copy(std::string(match[1]), ",", "."));
        n2 = std::stod(boost::replace_all_copy(std::string(match[2]), ",", "."));
        std::cout<< "GOT: " << n1 <<": " <<n2<<std::endl;
        if(n1 <= n2)
            return true;
    }
    return false;
}
int main()
{
    std::string str;
    while(!std::cin.eof()){
        std::getline(std::cin, str);
        std::cout<<"Is waluta? " <<((is_waluta(str))?"YES":"NO")<<std::endl;
        std::cout<<"Is donation? " <<((is_donation(str))?"YES":"NO")<<std::endl;
        std::cout<<"Is zapytanie? " <<((is_zapytanie(str))?"YES":"NO")<<std::endl;
    }
    return 0;
}
