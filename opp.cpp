/*
 * opp.cpp
 *
 * This file contains solution for the first task for JNP 2015/16
 *
 * Authors:
 * Bakalarski Piotr <pb358995@students.mimuw.edu.pl>
 * Krawczyk Przemysław <pk305178@students.mimuw.edu.pl>
 * Poskrobko Paweł <pawel.poskrobko@student.uw.edu.pl>
 *
 * Libraries used in this project:
 *  Boost (requires linking of boost_regex, boost_system and pthread)
 *  STL
 *
 * Basics:
 * We use uint64_t's to represent money values
 * as inegers - real_value = value/1000
 *
 */

/*
 * TODO:
 * Group functions by 'theme'(??)
 * Better comments(?)
 *
 */

#define DEBUG_OUTPUT 0

//====STL includes:
#include <string>
#include <istream>
#include <ostream>
#include <iostream>
#include <sstream>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include <cmath>
#include <limits>
#include <locale>
#include <cassert>
#include <cstdint>
#include <stdexcept>

//====Boost_asio includes:
#include <boost/asio.hpp>

//====Boost xml parser includes:
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

//====Boost regex includes:
#include <boost/regex.hpp>

//====Boost misc includes:
#include <boost/foreach.hpp>

//====Boost string inlcudes:
#include <boost/algorithm/string/replace.hpp>

using boost::asio::ip::tcp;

/*
 * Constants representing precision multipliers
 */
const int PRECISION_MUL = 1000;
const uint64_t PREC_POWS[4] = {1000, 100, 10, 1};

/*
 * Global variable to check if XML download succeeded
 */
bool is_xml_downloaded = false;

/*
 * Small helper function to help with checking
 * if multiplication will cause an overflow
 *
 */
size_t highestOneBitPosition(uint64_t a) {
    size_t bits=0;
    while (a!=0) {
        ++bits;
        a>>=1;
    };
    return bits;
}

/*
 * Multiply a and b - numbers in our special representation
 * And then round them to the 3rd decimal digit using bank rounding
 *
 * Throws std::overflow_error if multiplication would cause an overflow
 */
uint64_t round_multiply(uint64_t a, uint64_t b)
{
    uint64_t tmp = a*b;
    //Check if multiplication causes overflow
    if(highestOneBitPosition(a)+highestOneBitPosition(b) >64)
        throw std::overflow_error("Overflow douring multiplication");

    switch((tmp/(PRECISION_MUL/10))%10){
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
            return tmp/PRECISION_MUL;
        case 5:
            if((tmp/PRECISION_MUL)%2 == 0)
                return tmp/PRECISION_MUL;
            else
                return tmp/PRECISION_MUL + 1;
        case 6:
        case 7:
        case 8:
        case 9:
            return tmp/PRECISION_MUL + 1;
        default:
            assert(0);
    }
}

/*
 * Download contents of the file containing iso currency codes
 * and store it in a stringstream
 *
 * Returns 0 on success and <0 on failure
 */
int grabxml(std::ostream& os)
{
    const char *address = "www.currency-iso.org";
    const char *file = "/dam/downloads/lists/list_one.xml";
    //Resolve url
    boost::asio::io_service service;
    tcp::resolver resolver(service);
    tcp::resolver::query query(address, "http");
    tcp::resolver::iterator endpoint_it = resolver.resolve(query);
    tcp::resolver::iterator end;

    tcp::socket socket(service);
    boost::system::error_code error = boost::asio::error::host_not_found;
    while(error && endpoint_it !=end)
    {
        socket.close();
        socket.connect(*endpoint_it++, error);
    }

    if(error)// Could not connect. posible system error;
    {
        std::cerr << "Could not connect" << std::endl;
        std::cerr << "Fallback: treating any 3-letter code as "
            "a valid iso currency code!" << std::endl;
        return -1;
    }

    //Construct request
    boost::asio::streambuf request;
    std::ostream request_stream(&request);
    request_stream << "GET " << file <<" HTTP/1.0\r\n";
    request_stream << "Host: " << address <<"\r\n";
    request_stream << "Accept: */*\r\n";
    request_stream << "Connection: close\r\n\r\n";

    //Send the request
    boost::asio::write(socket, request);
    //Read the response
    boost::asio::streambuf response;
    boost::asio::read_until(socket, response, "\r\n");

    //Check if the response code is ok
    std::istream response_stream(&response);
    std::string http_version;
    response_stream >> http_version;
    unsigned int status_code;
    response_stream >> status_code;
    std::string status_message;
    std::getline(response_stream, status_message);

    //Notify user if connection can't be estabilished
    if (!response_stream || http_version.substr(0, 5) != "HTTP/")
    {
        std::cerr << "Invalid HTTP response code" << std::endl;
        std::cerr << "Fallback: treating any 3-letter code as "
            "a valid iso currency code!" << std::endl;
        return -1;
    }
    if (status_code != 200)
    {
        std::cerr << "Response returned with status code "
            << status_code << std::endl;
        std::cerr << "Fallback: treating any 3-letter code as "
            "a valid iso currency code!" << std::endl;
        return -1;
    }
    
    //Return code was 200 - success
    //Ignore the response headers
    //We do not need them
    boost::asio::read_until(socket, response, "\r\n\r\n");
    std::string header;

    while (std::getline(response_stream, header) && header != "\r");

    //Write (until EOF) our received data into provided stringstream
    if (response.size() > 0)
    {
        os << &response;
    }

    while (boost::asio::read(
                socket, response,boost::asio::transfer_at_least(1), error))
    {
        os << &response;
    }

    if (error != boost::asio::error::eof) //Error during reading!
    {
        std::cerr << "Error occured during response processing" << std::endl;
        std::cerr << "Fallback: treating any 3-letter code as "
            "a valid iso currency code!" << std::endl;
    }

    return 0;
}

/*
 * Parse the file containing xml data for iso currency codes
 * xml file contents are read from provided stringstream
 *
 * uses boost::ptree
 *
 * returns 0 on succesfull parse, <0 otherwise
 *
 *
 */
int parsexml(std::stringstream& ss, std::unordered_set<std::string>& set)
{
    using boost::property_tree::ptree;
    ptree pt;
    try
    {
        read_xml(ss, pt);
        BOOST_FOREACH(ptree::value_type const& v,
                pt.get_child("ISO_4217").get_child("CcyTbl"))
        {
            boost::optional<std::string> nm
                = v.second.get_optional<std::string>("Ccy");
            if(nm)
            {
                set.insert(*nm); // *nm -> get std:string from wrapper
            }
        }
    }
    catch(boost::property_tree::ptree_error &err)
    {
            return -1;
    }
    return 0;
}

/*
 * Parse a number in representation [0-9](,[0-9]{1,3})?
 * into our internal representation
 *
 * Throws std::out_of_range if the number is too large
 * to be parsed or stored in uint64_t
 *
 * Examples:
 * "123,45" -> 123450
 * "123,456" -> 123456
 * "123456" -> 123456000
 */

uint64_t string2number(const std::string& number)
{
    size_t comma_position = number.find(',');
    size_t digits_after_comma = 0; // 0 if no comma
    if (comma_position != std::string::npos)
    {
        digits_after_comma = number.length() - comma_position - 1;
    }

    uint64_t value = std::stoll(boost::replace_all_copy(number, ",", ""));
    uint64_t precision_multiplier = PREC_POWS[digits_after_comma];
    if(std::numeric_limits<uint64_t>::max() / precision_multiplier < value)
    {
        throw std::out_of_range("The number is too large");
    }
    return value * precision_multiplier;
}

/*
 * Helper function for printing numbers in our representation
 * Numbers are always printed in format:
 * (([1-9][0-9]*)|([0-9])),[0-9]{3}
 * 
 * Examples:
 * 12000 -> 12,000
 * 12345 -> 12,345
 * 12001 -> 12,001
 */
std::string number2string(const uint64_t& number)
{
    int fractional_part = number % 1000;

    // we want to have exactly 3 digits after the comma
    std::string delim = ",";
    if (fractional_part < 10)
    {
        delim = ",00";
    }
    else if (fractional_part < 100)
    {
        delim = ",0";
    }

    return std::to_string(number/1000) + delim
        + std::to_string(fractional_part);
}

/*
 * Check if the currency is already known.
 */
bool is_known_currency(const std::string& currency,
    const std::unordered_map<std::string, uint64_t>& known_currencies)
{
    return known_currencies.find(currency) != known_currencies.end();
}

/*
 * Check if the iso code of currency is valid.
 *
 * Also returns true if xml download failed - as fallback
 */
bool is_valid_isocode(const std::string& currency,
    const std::unordered_set<std::string>& isocodes)
{

    return is_xml_downloaded ?
        isocodes.find(currency) != isocodes.end()
        : true;
}

/*
 * Check if the loaded line represents a request to add a new currency.
 *
 * All correctness tests are done inside this function
 * Uses boost::regex, parsed values are returned through
 * referenced tuple
 */
bool is_currency(const std::string& line,
    const std::unordered_set<std::string>& isocodes,
    const std::unordered_map<std::string, uint64_t>& known_c,
    std::tuple<std::string, uint64_t>& currency_ratio)
{
    static const boost::regex rx(
            "^\\s*([A-Z]{3})\\s+(\\d+(?:,\\d{1,3})?)\\s*$");
    boost::smatch match;
    if (boost::regex_match(line, match, rx))
    {
        if (is_valid_isocode(std::string(match[1]), isocodes)
                && !is_known_currency(match[1], known_c))
        {
            try
            {
                std::get<0>(currency_ratio) = match[1];
                std::get<1>(currency_ratio) = string2number(match[2]);
                if(std::get<1>(currency_ratio) == 0)
                    return false;
            }
            catch (const std::out_of_range& oor)
            {
                return false;
            } 
            return true;
        }
    }
    return false;
}

/*
 * Check if the loaded line represents a request to add a new donation.
 * All correctness tests are done inside this function
 * Uses boost::regex, parsed values are returned through
 * referenced tuple
 */
bool is_donation(const std::string& line,
    const std::unordered_map<std::string, uint64_t>& currencies,
    std::tuple<std::string, uint64_t, std::string, uint64_t>& donation)
{
    static const boost::regex rx(
        "^\\s*([^\\s]+(?:\\s+[^\\s]+)*)\\s+(\\d+(?:,\\d{1,3})?)\\s+([A-Z]{3})\\s*$");

    boost::smatch match;
    if (boost::regex_match(line, match, rx))
    {
        if (is_known_currency(std::string(match[3]), currencies))
        {
            std::get<0>(donation) = match[1];
            try
            {
                std::get<1>(donation) = string2number(match[2]);
            }
            catch (const std::out_of_range& oor)
            {
                return false;
            }

            std::get<2>(donation) = match[3];
            try
            {
                uint64_t multiplier = currencies.find(std::get<2>(donation))->second;
                std::get<3>(donation) = round_multiply(
                    std::get<1>(donation), multiplier);
            }
            catch(const std::overflow_error& oe)
            {
                return false;
            }
            return true;
        }
    }
    return false;
}

/*
 * Check if the line is a query.
 * All correctness tests are done inside this function
 * Uses boost::regex, parsed values are returned through
 * referenced tuple
 */
bool is_query(const std::string& line,
    std::tuple<uint64_t, uint64_t>& lower_upper_bound)
{
    static const boost::regex rx(
            "^\\s*(\\d+(?:,\\d{1,3})?)\\s*(\\d+(?:,\\d{1,3})?)\\s*$");
    boost::smatch match;

    if(boost::regex_match(line, match, rx))
    {
        uint64_t n1, n2;
        try 
        {
            n1 = string2number(match[1]);
            n2 = string2number(match[2]);
        }
        catch (const std::out_of_range& oor)
        {
            return false;
        }

        if(n1 <= n2)
        {
            std::get<0>(lower_upper_bound) = n1;
            std::get<1>(lower_upper_bound) = n2;
            return true;
        }
    }
    return false;
}

/*
 * Helper function for printing errors to stderr in
 * desired format
 */
void print_error(const std::string& line, const long long int& line_number)
{
    std::cerr << "Error in line " << line_number << ":" << line << std::endl;
}

/*
 * Simple comparator function for sorting tuples containing donations
 */
bool compare_tuples(
    const std::tuple<std::string, uint64_t, std::string, uint64_t>& t1,
    const std::tuple<std::string, uint64_t, std::string, uint64_t>& t2)
{
    return std::get<3>(t1) < std::get<3>(t2);
}

/*
 * Processes the currencies file and adds new currencies
 * with calculated multipliers to the known_currencies map 
 * -when EOF or a correct line from a later file is detected,
 *  processing of this file ends, so the function is exited
 */
void process_currency_file(std::string& line,
    std::unordered_set<std::string>& isocodes,
    std::unordered_map<std::string, uint64_t>& known_currencies,
    long long int& line_number)
 {
    std::tuple<std::string, uint64_t, std::string, uint64_t> dummy1("", 0, "", 0);
    std::tuple<uint64_t, uint64_t> dummy2(0,0);

    std::tuple<std::string, uint64_t> currency_ratio("",0);
    do
    {
        if(line.length() == 0 && !std::cin)
        {
            return;
        }

        if(is_currency(line, isocodes, known_currencies, currency_ratio))
        {
                // new currency
                known_currencies.insert(std::pair<std::string, uint64_t>(
                            std::get<0>(currency_ratio),
                            std::get<1>(currency_ratio)));

                if(DEBUG_OUTPUT)
                {
                    std::cout << "ADD_CURRENCY (Name: " << std::get<0>(currency_ratio)
                        << ", multiplier: "
                        << number2string(std::get<1>(currency_ratio)) << ")\n";
                }
        }
        else if(is_donation(line, known_currencies, dummy1)) 
        {
                return;
        }
        else if (is_query(line, dummy2))
        {
                return;
        }
        else
        {
            print_error(line, line_number);
        }

        line_number++;
    } while(std::getline(std::cin, line));
 }

/*
 * Adds new donation to donations vector.
 * -when EOF or a correct line from a later file is detected,
 *  processing of this file ends, so the function is exited
 */
void process_donation_file(std::string& line,
        std::unordered_map<std::string, uint64_t>& known_currencies,
        std::vector<std::tuple<std::string, uint64_t, std::string, uint64_t>>& donations, 
        long long int& line_number)
{
    std::tuple<uint64_t, uint64_t> dummy(0,0);

    std::tuple<std::string, uint64_t, std::string, uint64_t> donation("", 0, "", 0);
    do {
        if (line.length() == 0 && !std::cin)
        {
            return;
        }

        if (is_donation(line, known_currencies, donation))
        {
            // Replaced long line using push_back
            // emplace_back calls the copy constructor
            donations.emplace_back(donation);
            
            if(DEBUG_OUTPUT)
            {
            std::cout << "ADD_DONATION (Name: " << std::get<0>(donation)
                      << ", amount: " << number2string(std::get<1>(donation)) 
                      << ", currency: " << std::get<2>(donation)
                      << ", value: " << number2string(std::get<3>(donation))
                      << ")\n";
            }
        }
        else if (is_query(line, dummy))
        {
            return;
        }
        else
        {
            print_error(line, line_number);
        }
        line_number++;
    } while (std::getline(std::cin, line));
}


/*
 * Helper functions for printing information
 * about a single donation as a response to a query
 */
void print_result_row(const std::tuple<std::string, uint64_t, std::string, uint64_t>& donation)
{
    std::cout << "\"" << std::get<0>(donation) << "\",\"" 
              << number2string(std::get<1>(donation))
              << "\","<< std::get<2>(donation) << std::endl;
}

/*
 * Processes all queries
 * -when EOF is detected, processing of this file ends,
 *  so the function is exited
 */
void process_queries(std::string& line,
        std::vector<std::tuple<std::string, uint64_t, std::string, uint64_t>>& donations,
        long long int& line_number)
{
    // Sort the donations file by real value of donations
    std::stable_sort(donations.begin(), donations.end(), compare_tuples);

    std::tuple<uint64_t, uint64_t> lower_upper_bound(0,0);
    do
    {
        if(line.length() == 0 && !std::cin)
        {
            return;
        }

        if(is_query(line, lower_upper_bound))
        {
            uint64_t lower = std::get<0>(lower_upper_bound);
            uint64_t upper = std::get<1>(lower_upper_bound);

            if(DEBUG_OUTPUT)
            {
            std::cout << "PROCESS_QUERY (Lower: " << number2string(lower)
                      << ", upper: " << number2string(upper) 
                      << ")\n";
            }
             
            auto it = std::lower_bound(donations.begin(), donations.end(),
                std::make_tuple("", 0, "", lower), compare_tuples);

            for (; it != donations.end(); ++it)
            {
                if (std::get<3>(*it) > upper) 
                {
                    break;
                }
                print_result_row(*it);
            }
        }
        else
        {
            print_error(line, line_number);
        }

        line_number++;
    } while(std::getline(std::cin, line));
}

int main()
{
    // set to store iso codes if xml download succeeded
    std::unordered_set<std::string> isocodes;

    // map of already provided currencies with their ratios
    std::unordered_map<std::string, uint64_t> known_currencies;

    // vector containg tuples with respectively: donator's name, donation, currency, value
    std::vector<std::tuple<std::string, uint64_t, std::string, uint64_t>> donations;

    // util string stream
    std::stringstream ss;

    //Download and parse the xml with isocodes
    if (grabxml(ss) == 0)
    {
        if (parsexml(ss, isocodes) == 0)
	    {
            is_xml_downloaded = true;
	    }
    }

    //For storing last read line
    std::string line;
    long long int line_number = 0;

    std::getline(std::cin, line);
    line_number++;

    process_currency_file(line, isocodes, known_currencies, line_number);
    process_donation_file(line, known_currencies, donations, line_number);
    process_queries(line, donations, line_number);

    return 0;
}
