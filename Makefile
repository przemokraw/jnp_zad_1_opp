#Makefile for OPP project
OBJS=opp.o
CXX=g++
CXXFLAGS=-Wall -g -Wextra -pedantic -Werror -std=c++11
LDFLAGS=-lboost_system -lpthread -lboost_regex

EXEFILE=opp.run

all: $(EXEFILE)

$(EXEFILE) : $(OBJS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

clean:
	rm -f $(EXEFILE) $(OBJS)
